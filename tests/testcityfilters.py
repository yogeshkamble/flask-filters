# Actual tests for city filters
import json
import nose
from nose.tools import *

from example import City, session

from tests import test_app


def check_content_type(headers):
  eq_(headers['Content-Type'], 'application/json')


def test_city_routes():
  # Test cases for city

  rv = test_app.get('/city')
  check_content_type(rv.headers)
  resp = json.loads(rv.data)

  # make sure we get a response
  eq_(rv.status_code, 200)
  # make sure there are no users
  eq_(len(resp), 0)

  # Add dummy city data

  pune_city = City(name='pune')
  mumbai_city = City(name='mumbai')
  mangalore_city = City(name='mangalore')

  session.add(pune_city)
  session.add(mumbai_city)
  session.add(mangalore_city)

  # Validate name filters
  rv = test_app.get('/city?name=pune')
  check_content_type(rv.headers)
  resp = json.loads(rv.data)

  eq_(rv.status_code, 200)
  eq_(len(resp), 1)

  # Tests response contain only pune city data
  eq_(resp[0]['name'], 'pune')
