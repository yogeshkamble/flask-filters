This package provide class Filters which you can override to add filter
for your flask resource.

Example

```
class City(Model):
    __tablename__='city'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(length=15), unique=True, nullable=False)
```

For the above model create filter class as follows

```
from flask_filters.filters import Filters

class CityFilter(Filters):

    model = City

    fields = ('name', )

    session = session # this will be your sqlalchemy session
```

Include the above filter to your CityResource class as follows

```
   city_response=dict(
       id=fields.Integer,
       name=fields.String,
    )
   class CityResource(Resource):

       filter_class = CityFilter

       @marshal_with(city_response)
       def get(self):
           filter_obj = self.filter_class()
           return filter_obj.get_results()
```

Register the resource

```
   api.add_resource(CityResource,'api/v1/city/')
```

using this you will have filter for name as follows

```
    api/v1/city/?name=pune
    api/v1/city/?name__like=mum # this will return all name which contain the word 'mum'
```

WARNING: Package is UNDER DEVELOPMENT

