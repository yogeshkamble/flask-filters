# Simple example for flask filters

import sqlite3
from flask import g, Flask
from flask_filters.flask_filters.filters import Filters

from sqlalchemy.ext.declarative import declarative_base
from flask_restful import Api, Resource, fields, marshal_with
from sqlalchemy import create_engine, Column, String, Integer, orm

Model = declarative_base()

DATABASE = 'sqlite:///test_database.db'
engine = create_engine(DATABASE,)
_Session = orm.sessionmaker(autocommit=False,autoflush=True,bind=engine)
session = orm.scoped_session(_Session)
Model.metadata.bind = engine
Model.query = session.query_property()

app = Flask(__name__)
api = Api(app)
app.debug = True


def init_db():
    Model.metadata.drop_all(bind=engine)
    Model.metadata.create_all(bind=engine)


# City Model
class City(Model):
    __tablename__='city'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(length=15), unique=True, nullable=False)

    def __repr__(self):
        return '<City %r>' % self.name


# City Filter
class CityFilter(Filters):
    """
    """
    model = City
    fields = ('name', )
    session = session

city_response=dict(
    id=fields.Integer,
    name=fields.String,
)


# City Resource
class CityResource(Resource):
    filter_class = CityFilter

    @marshal_with(city_response)
    def get(self):
        filter_obj = self.filter_class()
        return filter_obj.get_results()


#Flask Restful Routes
api.add_resource(CityResource, '/city')


@app.teardown_request
def teardown_request(exception):
    session.remove()